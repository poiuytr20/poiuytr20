Rails.application.routes.draw do
  devise_for :users
  root 'home#index'
  get '/home/index3' => 'home#index3'
  post 'home/create'
  get '/home/edit/:post_id' => 'home#edit'
  post '/home/update/:post_id' => 'home#update'
  post '/home/destroy/:post_id' => 'home#destroy'
  post '/home/reply' => 'reply#reply'
  post '/home/reply/destroy/:reply_id' => 'reply#destroy'
  post '/home/:reply_id/rereply' => 'rereply#create'
  post '/home/rereply/destroy/:rereply_id' => 'rereply#destroy'\
end
