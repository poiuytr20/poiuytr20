class CreateRereplies < ActiveRecord::Migration
  def change
    create_table :rereplies do |t|
      t.text :content
      t.integer :reply_id
      t.timestamps null: false
    end
  end
end
