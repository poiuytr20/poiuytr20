class Post < ActiveRecord::Base
    has_many :replies, dependent: :destroy
    belongs_to :user
end
