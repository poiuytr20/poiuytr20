class ReplyController < ApplicationController
    def reply
        Reply.create(content: params[:reply_content], post_id: params[:post_id])
        redirect_to :back
    end
  
    def destroy
        @reply = Reply.find(params[:reply_id])
        @reply.destroy
        redirect_to :back
    end
  
end
