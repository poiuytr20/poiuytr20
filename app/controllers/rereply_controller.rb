class RereplyController < ApplicationController
    def create
        Rereply.create(content: params[:rereply_content], reply_id: params[:reply_id])
        redirect_to :back
    end
  
    def destroy
        @rereply = Rereply.find(params[:rereply_id])
        @rereply.destroy
        redirect_to :back
    end
    
end
