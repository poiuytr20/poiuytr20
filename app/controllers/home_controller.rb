class HomeController < ApplicationController

  # Create
  
  def create
    uploader = HellUploader.new
    uploader.store!(params[:pic])
    Post.create(title: params[:title], content: params[:content], img_url: uploader.url)
    redirect_to "/"
  end

  # Read
  
  def index
    unless user_signed_in?
      redirect_to '/users/sign_in'
    end
    @posts = Post.all.reverse
  end
  
  def index2
    @posts = Post.all
  end
  # Update
  
  def edit
    @post = Post.find(params[:post_id])
  end
  
  def update
    uploader = HellUploader.new
    uploader.store!(params[:pic])
    @post = Post.find(params[:post_id])
    @post.content = params[:content]
    @post.img_url = uploader.url
    @post.save
    redirect_to "/"
  end
  
  # Destroy
  
  def destroy
    @post = Post.find(params[:post_id])
    @post.destroy
    redirect_to "/"
  end
  
end